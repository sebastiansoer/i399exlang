'use strict';



function add(x, y, callback) {
    callback(x + y);
}

function multiply(x, y, callback) {
    callback(x * y);
}
